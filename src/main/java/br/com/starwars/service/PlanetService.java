package br.com.starwars.service;

import br.com.starwars.domain.Planet;
import br.com.starwars.dto.PlanetRequestDTO;
import br.com.starwars.dto.PlanetResponseDTO;
import br.com.starwars.repository.PlanetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlanetService {

    @Autowired
    private PlanetRepository planetRepository;

    public List<PlanetResponseDTO> findAllPlanets() {
        return planetRepository.findAll()
                .stream()
                .map(PlanetResponseDTO::fromPlanet)
                .collect(Collectors.toList());
    }

    public PlanetResponseDTO findByIdPlanet(Long id) {
        Planet planet = planetRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Planeta não encontrado:" + id));
        return PlanetResponseDTO.fromPlanet(planet);
    }

    public void deletePlanetById(Long id) {
        planetRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Planeta não encontrado:" + id));
        planetRepository.deleteById(id);

    }

    public Optional<PlanetResponseDTO> updatePlanet(PlanetRequestDTO planetRequestDTO) {
        return planetRepository.findById(planetRequestDTO.getId())
                .map(planet -> {
                    planet.setName(planetRequestDTO.getName());
                    planet.setClimate(planetRequestDTO.getClimate());
                    planet.setTerrain(planetRequestDTO.getTerrain());
                    return planet;
                })
                .map(planetRepository::save)
                .map(PlanetResponseDTO::fromPlanet);
    }

    public PlanetResponseDTO savePlanet(PlanetRequestDTO planetRequestDTO) {
        return PlanetResponseDTO.fromPlanet(
                planetRepository.save(planetRequestDTO.buildPlanet())
        );

    }
}