package br.com.starwars.dto;

import br.com.starwars.domain.Planet;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PlanetResponseDTO {

    private Long id;
    private String name;
    private String climate;
    private String terrain;

    public static PlanetResponseDTO fromPlanet(Planet planet) {
        PlanetResponseDTO planetResponseDTO = new PlanetResponseDTO();
        planetResponseDTO.id = planet.getId();
        planetResponseDTO.name = planet.getName();
        planetResponseDTO.climate = planet.getClimate();
        planetResponseDTO.terrain = planet.getTerrain();
        return planetResponseDTO;
    }






}
