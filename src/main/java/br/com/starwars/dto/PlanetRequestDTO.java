package br.com.starwars.dto;

import br.com.starwars.domain.Planet;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PlanetRequestDTO {

    private Long id;
    private String name;
    private String climate;
    private String terrain;


    public Planet buildPlanet() {
        return Planet.builder()
                .name(this.name)
                .climate(this.climate)
                .terrain(this.terrain)
                .build();
    }
}
