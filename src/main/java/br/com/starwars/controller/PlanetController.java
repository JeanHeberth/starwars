package br.com.starwars.controller;

import br.com.starwars.dto.PlanetRequestDTO;
import br.com.starwars.dto.PlanetResponseDTO;
import br.com.starwars.service.PlanetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("planets")
public class PlanetController {

    @Autowired
    private PlanetService planetService;

    @GetMapping
    public ResponseEntity<List<PlanetResponseDTO>> findAllPlanets() {
        return ResponseEntity.ok(planetService.findAllPlanets());
    }

    @GetMapping("{id}")
    public ResponseEntity<PlanetResponseDTO> findByIdPlanet(@PathVariable Long id) {
        return ResponseEntity.ok(planetService.findByIdPlanet(id));
    }

    @DeleteMapping("{id}")
    public void deletePlanetById(@PathVariable Long id) {
        planetService.deletePlanetById(id);
    }

    @PostMapping
    public ResponseEntity<PlanetResponseDTO> savePlanet(@Validated @RequestBody PlanetRequestDTO planetRequestDTO) {
        return ResponseEntity.ok(planetService.savePlanet(planetRequestDTO));

    }
    @PutMapping
    public ResponseEntity<Optional<PlanetResponseDTO>> updatePlanet(@Validated @RequestBody PlanetRequestDTO planetRequestDTO){
        return ResponseEntity.ok(planetService.updatePlanet(planetRequestDTO));
    }
}
